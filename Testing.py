import os
import clip
import pandas as pd
import torch
from torchvision.datasets import CIFAR100
from config import CFG
from PIL import Image

# Download the dataset
cifar100 = CIFAR100(root=os.path.expanduser("~/.cache"), download=True, train=False)

# Prepare the inputs
image, class_id = cifar100[3637]

# Load the model
device = "cuda" if torch.cuda.is_available() else "cpu"
model, preprocess = clip.load('RN50', device)

# Prepare the inputs

image = Image.open("ADVISION_MAN_TopUsed_Gebrauchte_LK_10094311.jpg")
classes: object = pd.read_csv("ML-Dataset/all_labels.csv").values.tolist()

image_input = preprocess(image).unsqueeze(0).to(device)
text_inputs = torch.cat([clip.tokenize(f"a photo of a {c}") for c in classes]).to(device)

# Calculate features
with torch.no_grad():
    image_features = model.encode_image(image_input)
    text_features = model.encode_text(text_inputs)

# Pick the top 5 most similar labels for the image
image_features /= image_features.norm(dim=-1, keepdim=True)
text_features /= text_features.norm(dim=-1, keepdim=True)
similarity = (100.0 * image_features @ text_features.T).softmax(dim=-1)
values, indices = similarity[0].topk(5)

# Print the result
print("\nTop predictions:\n")
for value, index in zip(values, indices):
    print(classes[index])

    print(f"{100 * value.item():.2f}%")
    #print(f"{ classes[index]:>16s}: {100 * value.item():.2f}%")
