import torch
from transformers import BertTokenizer, BertModel
import numpy as np

# Load pre-trained BERT tokenizer and model
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')

# Example text descriptions
text_descriptions = [
    "A cute cat is playing with a ball.",
    "A friendly dog is wagging its tail.",
    "A colorful bird is perched on a branch.",
    "A colorful birde is pached onw as bas."

]

# Tokenize and encode the text descriptions
encoded_inputs = tokenizer(text_descriptions, padding=True, truncation=True, return_tensors='pt')

# Pass the input through the BERT model
with torch.no_grad():
    outputs = model(**encoded_inputs)

# Get the hidden states (embeddings) from BERT
text_embeddings = outputs.last_hidden_state  # You can apply pooling/aggregation as needed

text_embeddings= text_embeddings[:, 0, :]
# Convert PyTorch tensor to NumPy array for further processing
text_embeddings = text_embeddings.numpy()

# Now, text_embeddings contains the embeddings for each text description
