import os
import pandas as pd


## read the image name from the directory '''/Volumes/hka/Advision Data/001/Masterarbeit-Hamoud'''
## save them in a list of df
## for each image_name get the corresponding labels from motive_zu_designcodes
## create df [image][caption_number][caption][id]


## read the image name from the directory '''/Volumes/hka/Advision Data/001/Masterarbeit-Hamoud'''
def get_image_files(directory_path):
    # Define a set of image extensions to search for
    image_extensions = {".jpg", ".jpeg", ".png", ".bmp", ".gif", ".tif", ".tiff"}

    # Use a list comprehension to filter the files in the directory
    return [file for file in os.listdir(directory_path) if os.path.splitext(file)[1].lower() in image_extensions]


def build_multilabel(df, column, labels):
    # One-hot encode the 'rubricname' column
    df_dummies = pd.get_dummies(df[column])
    # Add the 'id_centralads' column to the dummies dataframe
    df_dummies['id_centralads'] = df['id_centralads']
    # Group by 'id_centralads' and sum the dummy columns
    df_grouped = df_dummies.groupby('id_centralads').sum()
    # Ensure that the values are either 0 or 1 (in case of multiple rows with the same rubricname for an id)
    df_grouped = (df_grouped > 0).astype(int)
    # Ensure the correct column order based on all_labels
    for label in labels:
        if label not in df_grouped.columns:
            df_grouped[label] = 0
    df_grouped = df_grouped[labels]
    # Reset the index
    df_grouped.reset_index(inplace=True)
    # Create the 'labels_rubricname' column
    df_grouped['labels'] = df_grouped[labels].values.tolist()

    # Group by 'id_centralads' in the original dataframe and aggregate captions
    captions_agg = df.groupby('id_centralads')[column].apply(lambda x: ','.join(set(x))).reset_index().astype(str)

    # Add the 'caption' column by merging with the original dataframe on 'id_centralads'
    df_grouped = df_grouped.merge(captions_agg, on='id_centralads', how='left')
    df_grouped.rename(columns={column: 'caption'}, inplace=True)

    # Drop the one-hot encoded columns
    df_grouped = df_grouped[['id_centralads', 'labels', 'caption']]

    df_grouped['id_centralads'] = df_grouped['id_centralads'].astype(str)
    df_grouped['id_centralads'] = df_grouped['id_centralads'] + '.jpg'
    df_grouped.rename(columns={'id_centralads': 'image'}, inplace=True)

    # Step 2: Reset the index
    df_grouped.reset_index(inplace=True)

    # Step 3: Rename the new column to 'id'
    df_grouped.rename(columns={'index': 'id'}, inplace=True)

    print(df_grouped.head(2))

    return df_grouped


directory_path = "/Volumes/hka/Advision Data/001/Masterarbeit-Hamoud"
images = get_image_files(directory_path)
motive_zu_design_df = pd.read_csv('/Volumes/hka/Advision Data/2022-10-10_Motive_zu_Designcodes.csv', delimiter=';',
                                  quotechar='"').astype(str)

# Create a DataFrame from the list of image names

# Create a DataFrame from the list of image names
image_df = pd.DataFrame(images, columns=['id_centralads']).astype(str)
image_df['id_centralads'] = image_df['id_centralads'].str.replace('.jpg', '', regex=False)
image_df['id_centralads'] = image_df['id_centralads'].astype(str)
image_df.to_csv('../ML-Dataset/image_names.csv', index=False)

motive_zu_design_df['id_centralads'] = motive_zu_design_df['id_centralads'].astype(float).astype('Int64').astype(str)
motive_zu_design_df = motive_zu_design_df.drop(columns=['id_rubric', 'id_aspect', 'id_element'])
filtered_motive_zu_design_df = motive_zu_design_df[motive_zu_design_df['id_centralads'].isin(image_df['id_centralads'])]
filtered_motive_zu_design_df = filtered_motive_zu_design_df.drop(columns=['id'])

filtered_motive_zu_design_df['combined'] = filtered_motive_zu_design_df.apply(
    lambda row: f"{row['rubricname']} und {row['aspectname']} und {row['elementname']}", axis=1)

rubricname_labels = list(set(filtered_motive_zu_design_df['rubricname']))
# aspectname_labels = list(set(filtered_motive_zu_design_df['aspectname']))
# elementname_labels = list(set(filtered_motive_zu_design_df['elementname']))
#
# print(rubricname_labels)
# print(aspectname_labels)
# print(elementname_labels)

df = pd.DataFrame(filtered_motive_zu_design_df)

filtered_motive_zu_design_all_labels_df = list(set(filtered_motive_zu_design_df['combined']))
filtered_motive_zu_design_rubric_df = list(set(filtered_motive_zu_design_df['rubricname']))

all_labels = pd.DataFrame(filtered_motive_zu_design_rubric_df)
all_labels.to_csv('../ML-Dataset/all_labels_rubric.csv', index=False)

# all_labels = pd.DataFrame(filtered_motive_zu_design_all_labels_df)
# all_labels.to_csv('../ML-Dataset/all_labels.csv', index=False)
#
# multilabel_all_df = build_multilabel(df, 'combined', filtered_motive_zu_design_df)
# multilabel_all_df.to_csv('../ML-Dataset/captions.csv', index=False)

#
multilabel_rubricname_df = build_multilabel(df, 'rubricname', rubricname_labels)
# multilabel_aspectname_df = build_multilabel(df, 'aspectname', aspectname_labels)
# multilabel_elementname_df = build_multilabel(df, 'elementname', elementname_labels)
#
# print(multilabel_rubricname_df)
# print(multilabel_aspectname_df)
# print(multilabel_elementname_df)
#

multilabel_rubricname_df.to_csv('../ML-Dataset/multilabel_rubricname.csv', index=False)
# multilabel_aspectname_df.to_csv('../ML-Dataset/multilabel_aspectname.csv', index=False)
# multilabel_elementname_df.to_csv('../ML-Dataset/multilabel_elementname.csv', index=False)
#
