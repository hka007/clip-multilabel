import math

import cv2
import numpy as np
import pandas as pd
from PIL import Image
from tqdm import tqdm
from transformers import DistilBertTokenizer

from CLIPDataset import get_transforms
from ProjectionHead import ProjectionHead
from Train import build_loaders, make_train_valid_dfs
from config import CFG
from nCLIPModel import CLIPModel
import torch

# Prepare the inputs

transforms = get_transforms()
image = cv2.imread("ADVISION_Deutsche_Post_ImageDTM_Deutsc_14043909.jpg")
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image = transforms(image=image)['image']
image = torch.tensor(image).permute(2, 0, 1).float()
image = image.unsqueeze(0)
classes: object = pd.read_csv("ML-Dataset/all_labels.csv").values.tolist()
tokenizer = DistilBertTokenizer.from_pretrained(CFG.text_tokenizer)

logit_scale = torch.tensor(math.log(1.0 / 0.5))
image_projection = ProjectionHead(embedding_dim=CFG.image_embedding)
text_projection = ProjectionHead(embedding_dim=CFG.text_embedding)


model = CLIPModel().to(CFG.device)
model.load_state_dict(torch.load("best.pt", map_location=CFG.device))
model.eval()



all_labels_df = pd.read_csv("ML-Dataset/all_labels.csv")
all_labels = all_labels_df.values
flattened_list = [item for sublist in all_labels for item in sublist]
text_inputs = tokenizer(flattened_list, padding=True, truncation=True, return_tensors='pt',
                           max_length=CFG.max_length)
# Calculate features
with torch.no_grad():
    image_features = model.image_encoder(image)
    text_features = model.text_encoder(input_ids=text_inputs['input_ids'], attention_mask=text_inputs['attention_mask'])

# Pick the top 5 most similar labels for the image
image_features /= image_features.norm(dim=-1, keepdim=True)
text_features /= text_features.norm(dim=-1, keepdim=True)

image_embeddings = image_projection(image_features)  # [n, d_e] [32,256]
text_embeddings = text_projection(text_features)  # [n_c, d_e] [32,256] should [334,256]

what = ((image_embeddings @ text_embeddings.T) / torch.exp(logit_scale)).to(CFG.device)

# scaled pairwise cos similarities [n, n_c]
similarity1 = torch.sigmoid(what)

values, indices = similarity1[0].topk(300)

 # Print the result
print("\nTop predictions:\n")
for value, index in zip(values, indices):
    print(classes[index])
    print(value.item())
