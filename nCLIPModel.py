import numpy as np
import torch.nn.functional as F
from torch import nn
from config import CFG
from ImageEncoder import ImageEncoder
from TextEncoder import TextEncoder
from ProjectionHead import ProjectionHead
import torch
import ast
import tensorflow as tf
import math

# image_encoder - vit
# text_encoder - Text Transformer
# I[n, h, w, c] - minibatch of aligned image
# T[n_c, L] - vector of tokenized label-text (n_c is #classes, l is tokens)
# W_i[d_i, d_e] - learned image proj
# W_t[d_t, d_e] - learned text proj
# targets[n, n_c] - the ground truth
# logit_scale  - the logit_scale param, = ln(1/temperature)
# extract feature representations of each modality
# I_f = image encoder(I) #[n, d_i]
# T_f = image encoder(T) #[n_c, d_t]
#
# joint multimodal embedding
# I_e = L2_normalize(np.dot(I_f, W_i), axis = 1) # [n, d_e]
# T_e = L2_normalize(np.dot(T_f, W_t), axis = 1) # [n_c, d_e]
#
# scaled pairwise cos similarities [n, n_c]
# logits = np.dot(I_e, T_e.T) * np.exp(logit_scale)
#
# loss - only need image level loss
# loss = nn.BCEWithLogitsLoss(logits, targets)
import tensorflow as tf
import numpy as np


def parse_string_to_tensor(s):
    # Remove brackets and split the string by comma
    s = s.strip('[]').split(',')
    # Convert the string elements to float and create a tensor
    tensor = np.array([float(x) for x in s], dtype=np.float32)
    return tensor


class CLIPModel(nn.Module):
    def __init__(
            self,
            temperature=CFG.temperature,
            image_embedding=CFG.image_embedding,
            text_embedding=CFG.text_embedding,
    ):
        super().__init__()
        self.image_encoder = ImageEncoder()
        self.text_encoder = TextEncoder()
        self.image_projection = ProjectionHead(embedding_dim=image_embedding)
        self.text_projection = ProjectionHead(embedding_dim=text_embedding)
        self.logit_scale = torch.tensor(math.log(1.0 / 0.5))

        self.log_softmax = nn.LogSoftmax(dim=-1)


    def forward(self, batch):
        I = batch['image']

        # T[n_c, 1] - vector of tokenized label-text (n_c is #classes, l is tokens)
        targets = batch["labels"]  # batch['targets']
        targets = np.stack([parse_string_to_tensor(s) for s in targets])
        targets = torch.tensor(targets).to(CFG.device)

        # extract feature representations of each modality
        image_features = self.image_encoder(I)  # [n, d_i] [32, 2048]
        input_ids = batch["input_ids"][0]
        attention_mask = batch["attention_mask"][0]

        text_features = self.text_encoder(
            input_ids=input_ids, attention_mask=attention_mask
        )  # [n_c, d_t] [32, 768] should [334,768]

        image_embeddings = self.image_projection(image_features)  # [n, d_e] [32,256]
        text_embeddings = self.text_projection(text_features)  # [n_c, d_e] [32,256] should [334,256]



        # scaled pairwise cos similarities [n, n_c]
        logits = ((image_embeddings @ text_embeddings.T) * torch.exp(self.logit_scale)).to(CFG.device)

        # Calculate the min and max values along each row (axis=1)
        min_values, _ = torch.min(logits, dim=1, keepdim=True)
        max_values, _ = torch.max(logits, dim=1, keepdim=True)

        scaled_logits = ((logits - min_values) / (max_values - min_values)).to(CFG.device)

        # loss - only need image level loss
        #loss = F.binary_cross_entropy(logits.to(CFG.device), targets.to(CFG.device))

        criterion = nn.BCEWithLogitsLoss()
        loss = criterion(scaled_logits, targets)

        return loss
