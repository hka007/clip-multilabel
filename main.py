import torch
import pandas as pd
import itertools
from tqdm.autonotebook import tqdm
from transformers import DistilBertTokenizer
from nCLIPModel import CLIPModel
from originalCLip import CLIP
from Train import make_train_valid_dfs, build_loaders
from Utils import AvgMeter, get_lr
from config import CFG

def train_epoch(model, train_loader, optimizer, lr_scheduler, step):
    loss_meter = AvgMeter()
    tqdm_object = tqdm(train_loader, total=len(train_loader))
    for batch in tqdm_object:
        #batch = {k: v.to(CFG.device) for k, v in batch.items() if k not in ["caption", "labels"]}
        batch = {k: (v.to(CFG.device) if k not in ["caption", "labels"] else v) for k, v in batch.items()}

        loss = model(batch)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if step == "batch":
            lr_scheduler.step()

        count = batch["image"].size(0)
        loss_meter.update(loss.item(), count)

        tqdm_object.set_postfix(train_loss=loss_meter.avg, lr=get_lr(optimizer))
    return loss_meter


def valid_epoch(model, valid_loader):
    loss_meter = AvgMeter()

    tqdm_object = tqdm(valid_loader, total=len(valid_loader))
    for batch in tqdm_object:
        # batch = {k: v.to(CFG.device) for k, v in batch.items() if k != "caption"}
        batch = {k: (v.to(CFG.device) if k not in ["caption", "labels"] else v) for k, v in batch.items()}

        loss = model(batch)

        count = batch["image"].size(0)
        loss_meter.update(loss.item(), count)

        tqdm_object.set_postfix(valid_loss=loss_meter.avg)
    return loss_meter


def main():
    train_df, valid_df = make_train_valid_dfs()
    tokenizer = DistilBertTokenizer.from_pretrained(CFG.text_tokenizer)
    all_labels_df = pd.read_csv("ML-Dataset/all_labels_rubric.csv")
    all_labels = all_labels_df.values
    flattened_list = [item for sublist in all_labels for item in sublist]

    encoded_labels = tokenizer(flattened_list, padding=True, truncation=True, return_tensors='pt',
                               max_length=CFG.max_length)

    #[:100]
    train_loader = build_loaders(train_df[:2000], tokenizer, encoded_labels, mode="train")
    valid_loader = build_loaders(valid_df[:1000], tokenizer, encoded_labels, mode="valid")

    model = CLIPModel().to(CFG.device)
    # model.load_state_dict(torch.load("best.pt", map_location=CFG.device))

    params = [
        {"params": model.image_encoder.parameters(), "lr": CFG.image_encoder_lr},
        {"params": model.text_encoder.parameters(), "lr": CFG.text_encoder_lr},
        {"params": itertools.chain(
            model.image_projection.parameters(), model.text_projection.parameters()
        ), "lr": CFG.head_lr, "weight_decay": CFG.weight_decay}
    ]
    optimizer = torch.optim.AdamW(params, weight_decay=0.)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, mode="min", patience=CFG.patience, factor=CFG.factor
    )
    step = "epoch"

    best_loss = float('inf')
    for epoch in range(CFG.epochs):
        print(f"Epoch: {epoch + 1}")
        model.train()
        train_loss = train_epoch(model, train_loader, optimizer, lr_scheduler, step)
        model.eval()
        with torch.no_grad():
            valid_loss = valid_epoch(model, valid_loader)

        if valid_loss.avg < best_loss:
            best_loss = valid_loss.avg
            torch.save(model.state_dict(), "best.pt")
            print("Saved Best Model!")

        lr_scheduler.step(valid_loss.avg)


if __name__ == '__main__':
    main()
